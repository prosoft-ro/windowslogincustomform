﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WindowsLoginCustomForm.Code;

namespace WindowsLoginCustomForm.Controllers
{
    public class TestController : Controller {

        public ActionResult Index() {

            var impersonator = (WindowsImpersonator)Session["WindowsImpersonator"];

            if (impersonator != null) {

                try {

                    impersonator.Impersonate();

                    //Check the username in the triggered log
                    using (var connection = new SqlConnection("Server=.;Database=StimulenteFinanciare;Trusted_Connection=True")) {
                        connection.Open();
                        var cmd = new SqlCommand("insert into Parametri (Id, Nume, Valoare) values (newid(), 'TestWindowsLoginCustomFormFromTest', 'TestWindowsLoginCustomFormFromTest')");
                        cmd.Connection = connection;
                        cmd.ExecuteNonQuery();
                    }

                } finally {
                    impersonator.UndoImpersonation();
                }

                return Content(User.Identity.Name);
            } else {
                return RedirectToAction("Index", "Login");
            }
        }
    }
}