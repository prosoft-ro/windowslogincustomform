﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WindowsLoginCustomForm.Code;

namespace WindowsLoginCustomForm.Controllers {

    public class LoginController : Controller {
        
        public ActionResult Index() {
            return View();
        }
            
        [HttpPost]
        public ActionResult Index(string username, string password) {

            var impersonator = new WindowsImpersonator();
            impersonator.Login(username, password);

            Session["WindowsImpersonator"] = impersonator;

            return RedirectToAction("Index", "Test");
        }
    }
}