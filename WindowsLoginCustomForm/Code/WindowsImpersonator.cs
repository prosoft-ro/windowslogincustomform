﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;

namespace WindowsLoginCustomForm.Code {

    /// <summary>
    /// Reference: https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ff647404(v=pandp.10)
    /// </summary>
    public class WindowsImpersonator : IDisposable {

        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_LOGON_NETWORK = 3;
        public const int LOGON32_LOGON_BATCH = 4;
        public const int LOGON32_LOGON_SERVICE = 5;
        public const int LOGON32_LOGON_UNLOCK = 7;
        public const int LOGON32_LOGON_NETWORK_CLEARTEXT = 8;
        public const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

        public const int LOGON32_PROVIDER_DEFAULT = 0;
        public const int LOGON32_PROVIDER_WINNT35 = 1;
        public const int LOGON32_PROVIDER_WINNT40 = 2;
        public const int LOGON32_PROVIDER_WINNT50 = 3;

        /// <summary>
        /// The pointer to the Windows Access Token.
        /// The token should not have an expiration date as it is not yet implemented.
        /// https://docs.microsoft.com/en-us/windows/desktop/api/Winnt/ns-winnt-_token_statistics
        /// </summary>
        public IntPtr TokenHandle { get; protected set; } = IntPtr.Zero;
        public WindowsIdentity ImpersonatedIdentity { get; protected set; }
        public WindowsImpersonationContext ImpersonationContext { get; protected set; }

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
            String lpszUsername,
            String lpszDomain,
            String lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken
        );

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        // If you incorporate this code into a DLL, be sure to demand that it runs with FullTrust.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static IntPtr GetWindowsToken(string username, string password, string domainName = ".") {

            // Use the unmanaged LogonUser function to get the user token for
            // the specified user, domain, and password.
            var tokenHandle = IntPtr.Zero;

            // Call LogonUser to obtain a handle to an access token.
            bool returnValue = LogonUser(
                username,
                domainName,
                password,
                LOGON32_LOGON_NETWORK_CLEARTEXT,
                LOGON32_PROVIDER_DEFAULT,
                ref tokenHandle
            ); // tokenHandle - new security token
            if (returnValue == false) {
                int ret = Marshal.GetLastWin32Error();
                throw new Win32Exception(ret);
            }

            return tokenHandle;
        }

        public WindowsImpersonator() { }
        public WindowsImpersonator(IntPtr tokenHandle) {
            TokenHandle = tokenHandle;
        }

        public void Login(string username, string password, string domainName = ".") {
            TokenHandle = GetWindowsToken(username, password, domainName);
        }

        public void Impersonate(string username, string password, string domainName = ".") {
            Login(username, password, domainName);
            Impersonate();
        }

        public void Impersonate() {
            ImpersonatedIdentity = new WindowsIdentity(TokenHandle);
            ImpersonationContext = ImpersonatedIdentity.Impersonate();
        }

        public void UndoImpersonation() {

            if (ImpersonationContext != null) {
                ImpersonationContext.Undo();
                ImpersonationContext.Dispose();
                ImpersonationContext = null;
            }
        }

        private bool isDisposed = false;
        public void Dispose() {

            if (isDisposed)
                return;

            UndoImpersonation();

            if (TokenHandle != IntPtr.Zero)
                CloseHandle(TokenHandle);

            isDisposed = true;
        }

        ~WindowsImpersonator() {
            Dispose();
        }
    }
}