﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WindowsLoginCustomForm.Code;

namespace WindowsLoginCustomForm
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_End(Object sender, EventArgs E) {

            var impersonator = (WindowsImpersonator)Session["WindowsImpersonator"];

            if (impersonator != null) {
                impersonator.Dispose();
            }
        }
    }
}
